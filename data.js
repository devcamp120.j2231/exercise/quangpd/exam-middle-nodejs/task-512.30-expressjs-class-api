class User{
    id;
    name;
    position;
    office;
    age;
    startDate;

    constructor(id,name,position,office,age,startDate){
        this.id = id;
        this.name = name;
        this.position = position;
        this.office = office;
        this.age = age;
        this.startDate = startDate;
    }
}

let User1 = new User("1","Airi Satou","Accountant","Tokyo",33,"2008/11/28");
let User2 = new User("2","Angelica Ramos","Chief Executive Officer (CEO)","London",44,"2009/10/09");
let User3 = new User("3","Ashton Cox","Junior Technical Author","San Francisco",66,"2009/01/12");
let User4 = new User("4","Bradley Greer","Software Engineer","London",41,"2012/10/13");
let User5 = new User("5","Brenden Wagner","Software Engineer","San Francisco",28,"2011/06/07");
let User6 = new User("6","Brielle Williamson","Integration Specialist","New York",61,"2012/12/02");
let User7 = new User("7","Bruno Nash","Software Engineer","London",38,"2011/05/03");
let User8 = new User("8","Caesar Vance","Pre-Sales Support","New York",21,"2011/12/12");
let User9 = new User("9","Cara Stevens","Sales Assistant","New York",46,"2011/12/06");
let User10 = new User("10","Cara Stevens","Senior Javascript Developer","Edinburgh",22,"2012/03/29");


const AllUser = [User1,User2,User3,User4,User5,User6,User7,User8,User9,User10];

module.exports = {AllUser}
