//Khai báo thư viện express
const express = require("express");
const { AllUser } = require("../../data");
const { userMiddleWare, getUserByIdMiddleWare, getUserByAgeMiddleWare } = require("../middlewares/userMiddleware");

//Khai báo router
const userRouter = express.Router();
//MiddleWare dùng chung cho cả router
userRouter.use(userMiddleWare);


//Get Object User By Age 
//localhost:8000/users?age=1
userRouter.get("/users",getUserByAgeMiddleWare,(req,res) =>{
    //B1 : thu thập dữ liệu
    let {age} = req.query;

    //B2: kiểm tra dữ liệu
   
    //B3 : Xử lý và trả về dữ liệu
    if(age){
        let UserObjByAge = AllUser.filter(function(item){
            return item.age > Number(age)
        })
        res.status(201).json({
            message : `Load User By Age Successfully`,
            UserObjByAge
        })
    }
    else{
        res.status(200).json({
            message : `Load All User Succesfully`,
            User : AllUser
        })
    }
 
})


//Get User By User Id 
userRouter.get("/users/:userId",getUserByIdMiddleWare,(req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;
    const User = AllUser.filter(function(item){
        return item.id === userId;
    })


    //B2 : kiểm tra dữ liệu
    if(!Number(userId)){
        return res.status(400).json({
            message :`Bad Request - userId is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    return res.status(201).json({
        message :`Get User By User Id Successfully`,
        user : User
    })
})

module.exports = {userRouter}