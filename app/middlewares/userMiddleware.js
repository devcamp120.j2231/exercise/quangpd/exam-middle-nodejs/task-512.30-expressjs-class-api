//MiddleWare For All 
const userMiddleWare = (req,res,next)=>{
    console.log("User Middleware - Time : " + new Date() + "Method : " + req.method);

    next();
}

//MiddleWare Get Object User By Age
const getUserByAgeMiddleWare = (req,res,next) =>{
    console.log("Get User By Age MiddleWare");

    next();
}

//MiddleWare Get User By UserId
const getUserByIdMiddleWare = (req,res,next)=>{
    console.log("Get User By User Id MiddleWare !");

    next();
}

module.exports = { userMiddleWare , getUserByIdMiddleWare ,getUserByAgeMiddleWare }