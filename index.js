//Khai báo thư viện express
const express = require("express");

//Khởi tạo app express
const app = express();

//khai báo router
const {userRouter} = require("./app/routes/userRouter");

//Khai báo cổng 
const port = 8000;



app.use("/",userRouter);

//Khai báo app tại port 8000
app.listen(port,() =>{
    console.log("App listening on port :" , port);
})